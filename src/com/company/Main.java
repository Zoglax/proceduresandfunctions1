
package com.company;

import java.util.Scanner;

//Tasks
public class Main {
    //For 5,6,7 tasks
    static void PrintStars(int n,int m)
    {
         for (int i=0;i<n;i++)
         {
             for (int j=0;j<m;j++)
             {
                 System.out.print("*");
             }
             System.out.println();
         }
         System.out.println();
    }

    //For 3,4 tasks
    static boolean IsChet(int number)
    {
         return number%2==0;
    }
    static void Enumeration(int a, int b)
    {
         while (a<=b)
         {
             if (IsChet(a))
             {
                 System.out.println(a);
             }
             a++;
         }
    }

    //For 1 task
    static int Descriminant(int a, int b, int c)
    {
        return b*b-4*a*c;
    }

    //For 2
    static int Fact(int n){
        int fact = 1;
        for (int i=1;i<=n;i++)
        {
            fact=fact*i;
        }
        return fact;
    }

    public static void main(String[] args) {
         Scanner scanner = new Scanner(System.in);
        //5
        //PrintStars(80);

        //6
        //int n=scanner.nextInt();
        //PrintStars(n);

        //7
        //int n=scanner.nextInt();
        //int m=scanner.nextInt();
        //PrintStars(n,m);

        //3
        /*int number;

        System.out.println("Input the number:");
        number = scanner.nextInt();

        if(IsChet(number))
        {
            System.out.println("Is even");
        }
        else
        {
            System.out.println("Is uneven");
        }*/

        //4
        /*int first, second;
        System.out.println("Input the first number:");
        first = scanner.nextInt();
        System.out.println("Input the second number:");
        second = scanner.nextInt();

        Enumeration(first,second);*/

        //1
        /*int a,b,c;
        System.out.println("Input the first number:");
        a = scanner.nextInt();
        System.out.println("Input the second number:");
        b = scanner.nextInt();
        System.out.println("Input the third number:");
        c = scanner.nextInt();

        System.out.println(Descriminant(a,b,c));*/

        //2

        int f4 = Fact(4);
        int f5 = Fact(5);
        int f6 = Fact(6);
        int f8 = Fact(8);

        int number=(2*f5+3*f8)/(f6+f4);

        System.out.println(number);
    }
}


